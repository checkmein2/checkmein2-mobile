# Getting started
## Installing Dependencies
You will need Node.js, the React Native command line interface, and Android Studio. In order to get this project started.

## Node 
Follow the [installation instructions for your Linux distribution](https://nodejs.org/en/download/package-manager/) install Node.js 4 or newer.

## The React Native CLI 
Node.js comes with npm, which lets you install the React Native command line interface.

Run the following command in a Terminal:

    npm install -g react-native-cli

If you get an error like `Cannot find module 'npmlog'`, try installing npm directly: `curl -0 -L https://npmjs.org/install.sh | sudo sh`.

## Android Development Environment 

### 1. Download and install Android Studio 
[Android Studio](https://developer.android.com/studio/install.html) provides the Android SDK and AVD (emulator) required to run and test your React Native apps. **Don't open the project before the next part since we need to configure the SDK Manager beforehand.**

### 2. Install the Android 6.0 (Marshmallow) SDK

Android Studio installs the most recent Android SDK by default. React Native, however, requires the `Android 6.0 (Marshmallow)` SDK. To install it, launch the SDK Manager, click on "Configure" in the "Welcome to Android Studio" screen.

Select "SDK Platforms" from within the SDK Manager, then check the box next to `Show Package Details`. Look for and expand the `Android 6.0 (Marshmallow)` entry, then make sure the following items are all checked:
* `Google APIs`
* `Android SDK Platform 23`
* `Sources for Android 23`
* `Intel x86 Atom_64 System Image`
* `Google APIs Intel x86 Atom_64 System Image`

Next, select the `SDK Tools` tab and check the box next to "Show Package Details" here as well. Look for and expand the `Android SDK Build Tools` entry, then make sure that Android SDK Build-Tools 23.0.1 is selected.

 Set up the ANDROID_HOME environment variable 

The React Native command line interface requires the ANDROID_HOME environment variable to be set up.

### 3. Add the following lines to your `~/.profile` (or equivalent) config file:

    export ANDROID_HOME=${HOME}/Android/Sdk
    export PATH=${PATH}:${ANDROID_HOME}/tools
    export PATH=${PATH}:${ANDROID_HOME}/platform-tools
Type `source ~/.profile` to load the config into your current shell.

Please make sure you export the correct path for `ANDROID_HOME` if you did not install the Android SDK using Android Studio.

### 4. Setting up you phone for development

In order to use your phone for development, you need to enable USB debugging on it. 

You first need to check if `Developer options` are enabled. You can find them somewhere at the bottom of the `Settings` application. If the `Developer options` don't show up, you need to enable them by going to `About phone` and tapping the `Build number` 7 times (on some versions of Android, the `Build number` might not be top level, but instead found in `Software Info`).

To enable USB debugging, you must go to `Settings > Developer options` and enable `USB Debugging`.

After enabling USB debugging, you will need to start the ADB (Android Device Bridge) daemon by running `adb devices`. Your device should show up with a status of unauthorized and a popup should show up on your phone with the title `Allow USB debugging?`. Be sure to check `Always allow from this computer` if you don't want to have to always allow this current PC.

## Testing the application on your phone

First you need to start the react native packer with:

        yarn start

Next you need to upload the application to your phone with:

        react-native run-android

If everything is set up correctly, you should see your this app running on your phone automatically (you don't have to start it). The application will also be installed under Apps. 

### Congratulations!! You've finished most of the boring part :)
### Hang in just a little more!

# Development tools

## IDE

I personally use [Visual Studio Code](https://code.visualstudio.com/), but any React + JSX configured IDE will do.

VSCode has a nice extension for [React Native](https://marketplace.visualstudio.com/items?itemName=vsmobile.vscode-react-native) with which you can simply start the project on you phone by selecting `React Native: Run Android` from the VSCode command palette (CTRL + SHIFT + P). It will also start the packer for you so it's all you have to do.

## OH NO! My application is throwing a runtime error and I have no idea how to debug it.

Fear not! If you are familiar with debugging normal Web React applications with Chrome Dev Tools, you are in luck!

There is a nice tool on github for [debugging react native apps](https://github.com/jhen0409/react-native-debugger).

It's easy to install and comes along with `React Inspector` and `Redux DevTools`

After installing it, just start it by running the `React Native Debugger` executable. After starting it, start the application on your device and shake it to bring up the `Developer menu`. Then select `Debug JS Remotely`.

**Hint: The `Developer menu` offers allot of cool stuff like `Live Reloading` and `Hot Reloading` and many other features. Be sure to check them out.**
# OK! I'm ready to have some fun!

If everything went right, you are ready to start development. 

`React Native` takes care of all the complicated Android / iOS stuff. You require almost no knowledge of native Android / iOS. 

The only folder we will use is the `src` folder, with the application main entry point in `src/index.js`.

From there on, it's all standard React + JSX syntax, only with some slight differences (instead of HTML tags, we use some react native components).

# I Can't login! You said I was ready!

I may have lied a little. While you are basically ready, you still need a social media account to login (Facebook, Twitter or Google+). 

## Facebook Login

### 1. I can't login because app is not in production mode.

Only users added to the application roles inside Facebook can login until the application will be in production mode. In order to do this, you need to ask someone to add you, or you can use the Facebook master account (you will still need to contact someone for it).

### 2. I logged in, it finally worked. But I changed something and I get some error about invalid `Key Hash`.

The application can only be changed by users that have their key hash registered in the application.

To generate a hash key, run:

        keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64

Now that you have a key, you need to add it to the facebook application. If you haven't been added as an administrator for the application, you need to find one and contact him to add it for you. If you have, congrats, you will add it yourself.

Open [facebook for developers](https://developers.facebook.com/). Go to checkmein2 application. Select `Settings` from the left navigation menu. Then scroll down to the Android main section and look for the `Key Hashes` field. Add yours at the end and **please make sure you don't remove any of them since you will cut some one else's access**. Click `Save Changes` in the lower right section.

### You should be all set with Facebook now.

## Twitter Login

TODO

### Google+ Login

TODO

# Now I'm serious. Go have some fun. You've earned it.