/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { AppRegistry, StyleSheet, View, UIManager } from 'react-native';
import App from 'index.js';

UIManager.setLayoutAnimationEnabledExperimental(true);

const checkmein2 = () => (
    <View
        style={style.viewStyle}>
        <App />
    </View>
);

const style = StyleSheet.create({
    viewStyle: {
        backgroundColor: 'black',
        height: '100%'
    }
});

export default checkmein2;
AppRegistry.registerComponent('checkmein2', () => checkmein2);
