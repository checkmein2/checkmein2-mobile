import * as ActionTypes from 'constants/ActionTypes';
import { AccessToken } from 'react-native-fbsdk';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';
import { currentUserQuery } from '../utils/Queries';

export const logout = () => dispatch => {
    AsyncStorage.removeItem('token');
    Actions.login();
    return dispatch({
        type: ActionTypes.LOGOUT
    });
};
export const login = (error, result, mutate) => dispatch => {
    if (error) {
        alert(`login has error: ${result.error}`);
    } else if (result.isCancelled) {
        alert('login is cancelled.');
    } else {
        AccessToken.getCurrentAccessToken().then(response => {
            const loginToken = response.accessToken.toString();
            mutate({
                variables: { access_token: loginToken },
                refetchQueries: [
                    {
                        query: currentUserQuery
                    }
                ]
            }).then(({ data }) => {
                AsyncStorage.setItem('token', data.userLogin.jwt);
                dispatch(loginSuccess());
                Actions.dashboard();
            });
        });
    }
    return dispatch({
        type: ActionTypes.LOGIN
    });
};
export const loginSuccess = () => ({
    type: ActionTypes.LOGIN_SUCCESS
});
