import { FACEBOOK_TOKEN_LOADED } from 'constants/ActionTypes';
import { AccessToken } from 'react-native-fbsdk';

const facebookTokenLoaded = (token) => ({
    type: FACEBOOK_TOKEN_LOADED,
    payload: token
});

export const loadFacebookToken = (dispatch) => {
    AccessToken.getCurrentAccessToken().then((token) => {
        dispatch(facebookTokenLoaded(token));
    });
};
