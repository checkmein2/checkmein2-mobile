import { ThemeProvider } from 'react-native-material-ui';
import { View, Text, StyleSheet, BackAndroid } from 'react-native';
import { LoginButton } from 'react-native-fbsdk';
import React, { Component } from 'react';

class LoginScene extends Component {
    componentWillMount() {
        // required to prevet android back button from returning to login/splash screen
        BackAndroid.addEventListener('hardwareBackPress', this.backHack);
    }
    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.backHack);
    }
    backHack() {
        return true;
    }
    render() {
        return (
            <ThemeProvider>
                <View style={styles.mainView}>
                    <View style={styles.descriptionsView}>
                        <Text> TODO: Some intro message </Text>
                    </View>
                    <View style={styles.buttonsView}>
                        <View style={{ flex: 0.4 }} />
                        <View style={styles.button}>
                            <LoginButton
                                publishPermissions={['publish_actions']}
                                onLoginFinished={this.props.onLogin}
                            />
                        </View>
                        <View style={{ flex: 0.4 }} />
                    </View>
                </View>
            </ThemeProvider>
        );
    }
}

LoginScene.propTypes = {
    onLogin: React.PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    descriptionsView: {
        flex: 0.5,
        justifyContent: 'center'
    },
    buttonsView: {
        flex: 0.5,
        backgroundColor: 'black',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        flex: 0.2,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default LoginScene;
