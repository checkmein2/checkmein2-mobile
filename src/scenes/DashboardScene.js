import { View, Text, StyleSheet, BackAndroid } from 'react-native';
import NavigationMenu from 'components/NavigationMenu';
import { Toolbar } from 'react-native-material-ui';
import { LoginButton } from 'react-native-fbsdk';
import React, { Component } from 'react';

class DashboardScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawer: false
        };
        this.backHack = this.backHack.bind(this);
        this.toggleDrawer = this.toggleDrawer.bind(this);
    }
    componentWillMount() {
        // required to prevet android back button from returning to login/splash screen
        BackAndroid.addEventListener('hardwareBackPress', this.backHack);
    }
    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.backHack);
    }
    backHack() {
        if (this.state.drawer) {
            this.setState({
                drawer: false
            });
        }
        return true;
    }
    toggleDrawer() {
        this.setState({
            drawer: !this.state.drawer
        });
    }
    render() {
        return (
            <View style={styles.mainView}>
                <Toolbar
                    leftElement="menu"
                    centerElement="News Feed"
                    onLeftElementPress={this.toggleDrawer}
                />
                <View style={styles.mainView}>
                    <View style={styles.descriptionsView}>
                        <Text> DASHBOAD </Text>
                    </View>
                    <View style={styles.buttonsView}>
                        <View style={{ flex: 0.4 }} />
                        <View style={styles.button}>
                            <LoginButton
                                publishPermissions={['publish_actions']}
                                onLoginFinished={() => {}}
                                onLogoutFinished={this.props.logout}
                            />
                        </View>
                        <View style={{ flex: 0.4 }} />
                    </View>
                </View>
                <NavigationMenu showMenu={this.state.drawer} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    descriptionsView: {
        flex: 0.5,
        justifyContent: 'center'
    },
    buttonsView: {
        flex: 0.5,
        backgroundColor: 'blue',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        flex: 0.2,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

DashboardScene.propTypes = {
    logout: React.PropTypes.func
};

export default DashboardScene;
