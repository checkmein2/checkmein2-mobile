import { View, Text } from 'react-native';
import React from 'react';
import { graphql } from 'react-apollo';
import { allCampaigns } from '../utils/Queries';
import Campaign from '../components/Campaign';

const CampaignsScene = props => {
    const { data } = props;
    return (
        <View>
            <Text> Campaigns </Text>
            <Campaign
                name={data.campaign.name}
                content={data.campaign.content}
            />
        </View>
    );
};

CampaignsScene.propTypes = {
    data: React.PropTypes.shape({
        loading: React.PropTypes.bool,
        campaign: React.PropTypes.object
    })
    // id: React.PropTypes.string
};

export default graphql(allCampaigns)(CampaignsScene);
