import { View, Text, StyleSheet } from 'react-native';
import { COLOR } from 'react-native-material-ui';
import React, { Component } from 'react';
class SplashScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timerExpired: false
        };
    }
    componentWillMount() {
        // this.props.loadFacebookToken();
        setTimeout(
            () => {
                this.setState({
                    timerExpired: true
                });
            },
            3000
        );
    }

    componentDidUpdate() {
        if (this.state.timerExpired) {
            if (this.props.loginToken) {
                this.props.navigateToDashboard();
            } else {
                this.props.navigateToLogin();
            }
        }
    }

    render() {
        return (
            <View style={styles.main}>
                <Text style={styles.title}> HELLO! </Text>
                <Text style={styles.text}> Welcome to the demo :) </Text>
            </View>
        );
    }
}

SplashScene.propTypes = {
    // loadFacebookToken: React.PropTypes.func,
    loginToken: React.PropTypes.string,
    navigateToDashboard: React.PropTypes.func,
    navigateToLogin: React.PropTypes.func
};

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: COLOR.blue100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 50,
        textAlign: 'center'
    },
    text: {
        fontSize: 15,
        textAlign: 'center'
    }
});

export default SplashScene;
