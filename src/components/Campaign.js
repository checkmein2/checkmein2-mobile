import React from 'react';
import {
    View, Text
} from 'react-native';

const Campaign = (props) => {
    const { name, content } = props;
    return (
        <View>
            <View>
                <Text> Name </Text>
                <Text> {name} </Text>
            </View>
            <View>
                <Text> Description </Text>
                <Text> {content} </Text>
            </View>
        </View>
    );
};
Campaign.propTypes = {
    name: React.PropTypes.string,
    content: React.PropTypes.string
};

export default Campaign;
