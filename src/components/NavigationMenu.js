import { View, Dimensions, Animated, Easing } from 'react-native';
import { Drawer, Subheader } from 'react-native-material-ui';
import React, { Component } from 'react';

const screenWidth = Dimensions.get('window').width;
const menuWidth = screenWidth * 0.8;

class NavigationMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuX: new Animated.Value(-menuWidth),
            overlayWidth: new Animated.Value(screenWidth)
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.showMenu !== nextProps.showMenu) {
            if (!nextProps.showMenu) {
                Animated.timing(this.state.menuX, {
                    toValue: -menuWidth,
                    duration: 250,
                    easing: Easing.linear
                }).start();
            } else {
                Animated.timing(this.state.menuX, {
                    toValue: 0,
                    duration: 250,
                    easing: Easing.linear
                }).start();
            }
        }
    }

    render() {
        const styles = {
            navigationMenu: {
                flex: 1,
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                paddingTop: 56,
                flexDirection: 'row'
            },
            drawer: {
                height: '100%',
                width: menuWidth,
                left: this.state.menuX
            },
            overlay: {
                height: '100%',
                width: screenWidth,
                opacity: 0.3,
                backgroundColor: 'black'
            }
        };

        return (
            <View style={styles.navigationMenu}>
                {this.props.showMenu &&
                    <View style={styles.navigationMenu}>
                        <View style={styles.overlay} />
                    </View>}
                <View style={styles.navigationMenu}>
                    <Animated.View style={styles.drawer}>
                        <Drawer>
                            <Drawer.Header>
                                <Subheader text="Checkmein2" />
                            </Drawer.Header>
                            <Drawer.Section
                                divider
                                items={[
                                    {
                                        icon: 'home',
                                        value: 'Home'
                                    },
                                    {
                                        icon: 'dashboard',
                                        value: 'Dashboard',
                                        active: true
                                    },
                                    {
                                        icon: 'pages',
                                        value: 'All Campaigns'
                                    },
                                    {
                                        icon: 'domain',
                                        value: 'All Challanges'
                                    }
                                ]}
                            />
                            <Drawer.Section
                                title="Personal"
                                items={[
                                    {
                                        icon: 'people',
                                        value: 'My Profile'
                                    },
                                    {
                                        icon: 'domain',
                                        value: 'My Challanges'
                                    }
                                ]}
                            />
                        </Drawer>
                    </Animated.View>
                </View>
            </View>
        );
    }
}

NavigationMenu.propTypes = {
    showMenu: React.PropTypes.bool
};

export default NavigationMenu;
