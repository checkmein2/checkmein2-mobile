import * as ActionTypes from 'constants/ActionTypes';

const initialState = {
    isLoggesIn: false
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case ActionTypes.LOGIN_SUCCESS:
            return Object.assign(state, { isLoggesIn: true });
        case ActionTypes.LOGOUT:
            return Object.assign(state, { isLoggesIn: false });
        default:
            return state;
    }
}
