import { createNetworkInterface } from 'apollo-client';
import UserReducer from 'reducers/UserReducer';
import { ApolloClient } from 'react-apollo';
import { combineReducers } from 'redux';
import routes from 'reducers/routes';
import { AsyncStorage } from 'react-native';

const networkInterface = createNetworkInterface({
    uri: 'http://localhost:8000/graphql',
    transportBatching: true
});
/*eslint-disable*/
networkInterface.use([
    {
        applyMiddleware(req, next) {
            if (!req.options.headers) {
                req.options.headers = {}; // Create the header object if needed.
            }
            AsyncStorage.getItem('token', (err, result) => {
                req.options.headers.authorization = result;
            });
            next();
        }
    }
]);
/*eslint-enable*/

const client = new ApolloClient({
    networkInterface
});

const reducers = combineReducers({
    routes,
    apollo: client.reducer(),
    UserReducer
});

export { client, reducers };
