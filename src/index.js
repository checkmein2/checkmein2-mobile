import DashboardSceneContainer from 'containers/DashboardSceneContainer';
import CampaignsSceneContainer from 'containers/CampaignsSceneContainer';
import SplashSceneContainer from 'containers/SplashSceneContainer';
import { Router, Scene, Actions } from 'react-native-router-flux';
import LoginSceneContainer from 'containers/LoginSceneContainer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ThemeProvider } from 'react-native-material-ui';
import { createStore, applyMiddleware } from 'redux';
// import { AccessToken } from 'react-native-fbsdk';
import { connect, Provider } from 'react-redux';
import { ApolloProvider } from 'react-apollo';
import { reducers, client } from 'reducers';
import React from 'react';
import thunk from 'redux-thunk';

const RouterWithProps = props => <Router hideNavBar {...props} />;

const RouterWithRedux = connect()(RouterWithProps);

const store = createStore(
    reducers,
    {},
    composeWithDevTools(applyMiddleware(thunk, client.middleware()))
);

// Scenes are created here to prevent recreation when redux triggers a render for the router
const scenes = Actions.create(
    <Scene key="root">
        <Scene key="splash" component={SplashSceneContainer} initial />
        <Scene key="login" component={LoginSceneContainer} />
        <Scene
            key="dashboard"
            backAndroidHandler={() => true}
            component={DashboardSceneContainer}
        />
        <Scene
            key="campaigns"
            backAndroidHandler={() => true}
            component={CampaignsSceneContainer}
        />
    </Scene>
);

export default () => (
    <ThemeProvider>
        <ApolloProvider client={client}>
            <Provider key="provider" store={store}>
                <RouterWithRedux duration={0} scenes={scenes} />
            </Provider>
        </ApolloProvider>
    </ThemeProvider>
);
