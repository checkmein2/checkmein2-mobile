import gql from 'graphql-tag';

export const userLogin = gql`
    mutation userLogin($access_token: String!) {
        userLogin(access_token: $access_token) {
          jwt
        }
    }
`;
