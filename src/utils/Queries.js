import gql from 'graphql-tag';

export const currentUserQuery = gql`
  query currentUserData {
    currentUser {
      name,
      email,
      id,
      user_role
    }
  }
`;
export const allCampaignsQuery = gql` query allCampaignsData {
    campaigns {
      name,
      content
    }
  }
`;
