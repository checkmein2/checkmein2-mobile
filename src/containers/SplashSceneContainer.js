import { getLoginToken } from 'selectors/UserSelectors';
import { Actions } from 'react-native-router-flux';
import { loadFacebookToken } from 'actions/UserActions';
import SplashScene from 'scenes/SplashScene';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    loginToken: getLoginToken(state)
});

const mapDispatchToProps = dispatch => ({
    navigateToLogin: () => Actions.login(),
    navigateToDashboard: () => Actions.dashboard(),
    loadFacebookToken: () => loadFacebookToken(dispatch)
});

const wrapper = connect(mapStateToProps, mapDispatchToProps);

export default wrapper(SplashScene);
