import { login } from 'actions/LoginActions';
import { graphql, compose, withApollo } from 'react-apollo';
import CampaignsScene from 'scenes/CampaignsScene';
import { connect } from 'react-redux';
import { allCampaignsQuery } from '../utils/Queries';

const mapDispatchToProps = (dispatch, ownProps) => ({
    onLogin: (error, result) => {
        dispatch(login(error, result, ownProps.mutate));
    }
});

const wrapper = compose(
    withApollo,
    graphql(allCampaignsQuery),
    connect(null, mapDispatchToProps)
);

export default wrapper(CampaignsScene);
