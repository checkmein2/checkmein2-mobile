import { login } from 'actions/LoginActions';
import { graphql, compose, withApollo } from 'react-apollo';
import LoginScene from 'scenes/LoginScene';
import { connect } from 'react-redux';
import { userLogin } from '../utils/Mutations';

const mapDispatchToProps = (dispatch, ownProps) => ({
    onLogin: (error, result) => {
        dispatch(login(error, result, ownProps.mutate));
    }
});

const wrapper = compose(
    withApollo,
    graphql(userLogin),
    connect(null, mapDispatchToProps)
);

export default wrapper(LoginScene);
