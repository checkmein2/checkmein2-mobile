import DashboardScene from 'scenes/DashboardScene';
import { logout } from 'actions/LoginActions';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(logout())
});

const wrapper = connect(null, mapDispatchToProps);

export default wrapper(DashboardScene);
